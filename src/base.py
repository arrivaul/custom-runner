# Standard library
import logging
import logging.handlers
import math
import os
import shlex
import socket
import sys
import traceback
import typing

# Third-party imports
import cs
import fabric
import paramiko.ssh_exception
import pydantic
import requests
import tenacity
import typing_extensions
import yaml

GITLAB_API_ENDPOINT = "https://gitlab.inria.fr/api/v4"
EMAIL_ADMIN = "pfo-staff@inria.fr"

VirtualMachine = dict[str, typing.Any]


class Spec(pydantic.BaseModel):
    cpu: int
    ram: int
    disk: int

    def adjust_for_template(
        self, logger: logging.LoggerAdapter, template, log: bool
    ) -> "Spec":
        template_size = int(math.ceil(int(template["size"]) / (1024**3)))
        if template_size <= self.disk:
            return self
        if log:
            template_name = template["name"]
            logger.warning(
                f"Virtual machine will have {template_size} GB for root disk instead of {self.disk} GB because the template {template_name} requires it."
            )
        return Spec(cpu=self.cpu, ram=self.ram, disk=template_size)

    @classmethod
    def sum(cls, specs: list["Spec"]) -> "Spec":
        return Spec(
            cpu=sum(spec.cpu for spec in specs),
            ram=sum(spec.ram for spec in specs),
            disk=sum(spec.disk for spec in specs),
        )

    def is_negative_or_null(self) -> bool:
        return self.cpu <= 0 and self.ram <= 0 and self.disk <= 0

    def add(self, other: "Spec") -> "Spec":
        return Spec(
            cpu=self.cpu + other.cpu,
            ram=self.ram + other.ram,
            disk=self.disk + other.disk,
        )

    def sub(self, other: "Spec") -> "Spec":
        return Spec(
            cpu=self.cpu - other.cpu,
            ram=self.ram - other.ram,
            disk=self.disk - other.disk,
        )


DEFAULT_SPEC = Spec(cpu=2, ram=8, disk=16)


class Options(pydantic.BaseModel):
    docker: typing.Optional[str]


DEFAULT_OPTIONS = Options(docker=None)


class CloudStack(pydantic.BaseModel):
    kind: typing.Literal["CloudStack"]
    template_filter: str
    template_name: str
    spec: Spec
    options: Options


class Test(pydantic.BaseModel):
    kind: typing.Literal["Test"]
    name: str


class Image(pydantic.BaseModel):
    contents: typing_extensions.Annotated[
        CloudStack | Test, pydantic.Field(discriminator="kind")
    ]


MAX_CPU = 16

MAX_RAM = 24

MAX_DISK = 100


class JobInfo(pydantic.BaseModel):
    project_path: str
    job_id: str
    job_token: str
    image: Image


class DestroyOrder(pydantic.BaseModel):
    job_info: JobInfo
    vm_name: str


def get_job_info() -> JobInfo:
    return JobInfo(
        project_path=os.environ["CUSTOM_ENV_CI_PROJECT_PATH"],
        job_id=os.environ["CUSTOM_ENV_CI_JOB_ID"],
        job_token=os.environ["CUSTOM_ENV_CI_JOB_TOKEN"],
        image=Image.model_validate_json(os.environ["image"]),
    )


def get_cache_dir(job_info: JobInfo) -> str:
    project_path = job_info.project_path
    cache_dir = f"/mnt/cache/{project_path}"
    return cache_dir


class CloudStackConfig(pydantic.BaseModel):
    endpoint: str
    key: str
    secret: str
    timeout: typing.Optional[int] = None


class Config(pydantic.BaseModel):
    cloudstack: CloudStackConfig
    zone: str
    cpu_speed: typing.Optional[int] = None

    def get_cloudstack(self) -> cs.CloudStack:
        return cs.CloudStack(**self.cloudstack.dict())

    @classmethod
    def load(cls) -> "Config":
        # We do not use Config.parse_file since it seems to become
        # deprecated.
        # https://github.com/pydantic/pydantic/issues/136
        with open("/builds/config.yml") as config_file:
            return Config.model_validate(yaml.safe_load(config_file))


def make_connection_in_stream_fast(connection: fabric.Connection):
    """
    Setting input_sleep = 0, otherwise connection.run(in_stream=...) is slow.
    See https://github.com/pyinvoke/invoke/issues/774
    """
    connection.config.runners.remote.input_sleep = 0


def execute(command: list[str]) -> None:
    assert os.waitstatus_to_exitcode(os.system(shlex.join(command))) == 0


def get_vm_ip_address(vm) -> str:
    return vm["nic"][0]["ipaddress"]


def find_template(cloudstack: cs.CloudStack, image: CloudStack):
    templates = cloudstack.listTemplates(
        templateFilter=image.template_filter, name=image.template_name, fetch_list=True
    )
    if len(templates) == 0:
        raise Exception(f"No such template {image.template_name}")
    if len(templates) >= 2:
        ids = ", ".join([template["id"] for template in templates])
        raise Exception(f"Ambiguous template {image.template_name}: {ids}")
    return templates[0]


def notify_vm_deployment_disabled(logger: logging.LoggerAdapter):
    logger.exception(
        f"CloudStack deployment is disabled. Please contact a CI administrator: {EMAIL_ADMIN}. Sorry for the inconvenience."
    )
    exit(1)


VM_DEPLOYMENT_DISABLED_FILE = "/builds/vm_deployment_disabled"


def deploy_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    config: Config,
    vm_name,
    spec: Spec,
    template,
):
    """
    Deploy a VM given `spec` and `template` on `cloudstack`.
    If deployment is disabled (i.e., if there is a file
    `/builds/vm_deployment_disabled`), execution is interrupted.
    Return an async job, to be waited with `wait_for_vm_deployment`.
    """
    if os.path.exists(VM_DEPLOYMENT_DISABLED_FILE):
        notify_vm_deployment_disabled(logger)
    details = {"cpuNumber": spec.cpu, "memory": spec.ram * 1024}
    if config.cpu_speed:
        details["cpuSpeed"] = config.cpu_speed
    kwargs = {
        "serviceOfferingId": get_service_offering_id(cloudstack, "Custom"),
        "templateId": template["id"],
        "zoneId": get_zone_id(cloudstack, config.zone),
        "name": vm_name,
        "displayName": vm_name,
        "details": details,
        "rootDiskSize": spec.disk,
    }
    # VMware hypervisors do not support security groups (VM isolation for VMware
    # is ensured by setting up the firewall, see prepare.init_pf).
    if template["hypervisor"] != "VMware":
        kwargs["securityGroupNames"] = ["runners"]
    return cloudstack.deployVirtualMachine(**kwargs)


def wait_for_vm_deployment(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, job
):
    """
    Wait for a VM to be deployed, returning the VM record once the VM is
    deployed.
    CloudStack error `530 Insufficient capacity` is caught to block any
    subsequent VM deployment before a manual intervention (a file
    /builds/vm_deployment_disabled is created: while such a file exists,
    `deploy_vm` immediately breaks the script).
    Other errors are propagated to the caller.
    See https://gitlab.inria.fr/inria-ci/custom-runner/-/issues/17
    """
    try:
        return cloudstack.queryAsyncJobResult(jobId=job["jobid"], fetch_result=True)[
            "virtualmachine"
        ]
    except cs.client.CloudStackApiException as exc:
        error_code = exc.error["errorcode"]
        extra = {"cloudstack_error_code": error_code}
        if error_code == 530:
            logger.exception(f"CloudStack insufficient capacity: {exc}.", extra=extra)
            with open(VM_DEPLOYMENT_DISABLED_FILE, "w") as _f:
                pass
            notify_vm_deployment_disabled(logger)
        else:
            raise exc


def get_service_offering_id(cloudstack: cs.CloudStack, name: str) -> str:
    response = cloudstack.listServiceOfferings(name=name, fetch_list=True)
    return response[0]["id"]


def get_zone_id(cloudstack: cs.CloudStack, name: str) -> str:
    response = cloudstack.listZones(name=name, fetch_list=True)
    return response[0]["id"]


def get_destroy_socket_uri() -> str:
    return "ipc:///builds/destroy.socket"


def build_tags(tags: dict[str, str]) -> dict[str, str]:
    """
    Prepare tags arguments for CloudStack.
    >>> build_tags({ "job": "test" })
    {'tags[0].key': 'job', 'tags[0].value': 'test'}
    """
    result = {}
    for i, (key, value) in enumerate(tags.items()):
        result[f"tags[{i}].key"] = key
        result[f"tags[{i}].value"] = value
    return result


JOB_ID_TAG_KEY = "job_id"

JOB_TOKEN_TAG_KEY = "job_token"

RUNNER_TAG_KEY = "runner"


def list_virtual_machines(cloudstack: cs.CloudStack, **kwargs) -> list[VirtualMachine]:
    # Since 4.17, name parameter does exact string match (contrary to the documentation:
    # "a substring match is made against the parameter value",
    # see https://cloudstack.apache.org/api/apidocs-4.17/apis/listVirtualMachines.html).
    # We should now use the `keyword` parameter to filter on prefix.
    # See https://github.com/apache/cloudstack/pull/6032
    if prefix := kwargs.pop("prefix", None):
        kwargs["keyword"] = prefix
    vm_list = cloudstack.listVirtualMachines(fetch_list=True, **kwargs)
    if prefix:
        vm_list = [vm for vm in vm_list if vm["name"].startswith(prefix)]
    if name := kwargs.get("name"):
        # Until 4.16 included, CloudStack does substring match. We ensure that the
        # exact name is matched.
        vm_list = [vm for vm in vm_list if vm["name"] == name]
    return vm_list


class VirtualMachineNotFound(Exception):
    pass


def find_vm_by_job(cloudstack: cs.CloudStack, job_info: JobInfo):
    vm_list = list_virtual_machines(
        cloudstack, **build_tags({JOB_ID_TAG_KEY: job_info.job_id})
    )
    if len(vm_list) > 1:
        raise VirtualMachineNotFound(
            f"Multiple virtual machines for job {job_info.job_id}"
        )
    try:
        return vm_list[0]
    except IndexError:
        raise VirtualMachineNotFound(f"No virtual machine for job {job_info.job_id}")


def get_vm_by_id(cloudstack: cs.CloudStack, id: str):
    return next(iter(list_virtual_machines(cloudstack, id=id)), None)


def find_vm_by_id(cloudstack: cs.CloudStack, id: str):
    result = get_vm_by_id(cloudstack, id)
    if result is None:
        raise VirtualMachineNotFound(f"No virtual machine with id {id}")
    return result


def get_vm_by_name(cloudstack: cs.CloudStack, name: str):
    vm_list = list_virtual_machines(cloudstack, name=name)
    if len(vm_list) > 1:
        raise VirtualMachineNotFound(f"Multiple virtual machines with name {name}")
    return next(iter(vm_list), None)


def find_vm_by_name(cloudstack: cs.CloudStack, name: str):
    result = get_vm_by_name(cloudstack, name)
    if result is None:
        raise VirtualMachineNotFound(f"No virtual machine with name {name}")
    return result


def has_tag(vm, key: str) -> bool:
    return any(tag["key"] == key for tag in vm["tags"])


def get_tag(vm, key: str) -> typing.Optional[str]:
    return next((tag["value"] for tag in vm["tags"] if tag["key"] == key), None)


def custom_before_sleep_log(
    logger_adapter: logging.LoggerAdapter, log_level: int
) -> typing.Callable[[tenacity.RetryCallState], None]:
    """Custom before_sleep function to support LoggerAdapter."""

    def log(retry_state):
        if retry_state.attempt_number < 1:
            return
        logger_adapter.log(
            log_level, "Retrying... Attempt %s", retry_state.attempt_number
        )

    return log


def is_cloudstack_internal_error(exception):
    """Check if the given error corresponds to a CloudStack internal error."""
    # Unfortunately, I see no way to distinguish internal errors from
    # tag conflicts (the "else" case below), but by looking on the
    # error message.  In case of "Internal error", the error message
    # is "Internal error executing command, please contact your system
    # administrator" and in case of tag conflict, the error message is
    # of the form "tag <name> already on UserVm with id <id>"
    return (
        isinstance(exception, cs.client.CloudStackApiException)
        and exception.error["errorcode"] == 530
        and exception.error["errortext"].startswith("Internal error")
    )


def is_connection_error(exception):
    """Check if the given error corresponds to a connection error."""
    return (
        isinstance(exception, paramiko.ssh_exception.SSHException)
        or isinstance(exception, paramiko.ssh_exception.AuthenticationException)
        or isinstance(exception, paramiko.ssh_exception.ChannelException)
        or isinstance(exception, paramiko.ssh_exception.NoValidConnectionsError)
        or isinstance(exception, socket.gaierror)
        or isinstance(exception, TimeoutError)
        or isinstance(exception, EOFError)
    )


def create_vm_tags(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vm, tags: dict[str, str]
) -> None:
    # https://gitlab.inria.fr/inria-ci/custom-runner/-/issues/2
    # Some "Internal error"s occurred when setting tags.
    # I see no better solution than retrying in that case.
    @tenacity.retry(
        retry=tenacity.retry_if_exception(is_cloudstack_internal_error),
        stop=tenacity.stop_after_attempt(10),
        wait=tenacity.wait_exponential(max=60),
        before_sleep=custom_before_sleep_log(logger, logging.WARNING),
    )
    def core(cloudstack, vm, tags):
        # We first check that the tags are not already set,
        # which can happen after CloudStack internal error.
        vm = find_vm_by_id(cloudstack, vm["id"])
        new_tags = {
            key: value for key, value in tags.items() if get_tag(vm, key) != value
        }
        if new_tags != {}:
            cloudstack.createTags(
                resourceIds=[vm["id"]],
                resourceType="UserVM",
                fetch_result=True,
                **build_tags(new_tags),
            )

    return core(cloudstack, vm, tags)


def try_create_vm_tags(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vm, tags: dict[str, str]
) -> bool:
    try:
        create_vm_tags(logger, cloudstack, vm, tags)
    except cs.client.CloudStackApiException as e:
        if not is_cloudstack_internal_error(e):
            vm_name = vm["name"]
            keys = ", ".join(tags.keys())
            logger.warning(
                f"Some tags among {keys} already set to a different value on virtual machine {vm_name}",
                exc_info=True,
            )
            return False
        else:
            raise e
    # The VM may have been deleted between the call to
    # `listVirtualMachines` and the call to `try_create_vm_tags`, and we
    # expect this function to return `False` to let the caller look for
    # another VM instead of failing.
    # For instance, https://gitlab.inria.fr/coq/coq/-/jobs/3437173
    except VirtualMachineNotFound:
        vm_name = vm["name"]
        keys = ", ".join(tags.keys())
        logger.warning(
            f"Virtual machine {vm_name} not found while setting tags {keys}",
            exc_info=True,
        )
        return False
    return True


def delete_vm_tags(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vm, tags: dict[str, str]
) -> None:
    vm_name = vm["name"]
    logger.info(f"Remove tags {tags} from virtual machine {vm_name}")
    cloudstack.deleteTags(
        resourceIds=[vm["id"]],
        resourceType="UserVM",
        fetch_result=True,
        **build_tags(tags),
    )


JOB_PREPARE_KEY = "prepare"

JOB_PREPARE_DESTROY_MARK = "destroy"


def destroy_vms(logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vms) -> None:
    if len(vms) == 0:
        return
    vm_names = ", ".join(vm["name"] for vm in vms)
    if len(vms) == 1:
        items = "virtual machine"
    else:
        items = "virtual machines"
    logger.warning(f"Destroying {items} {vm_names}.")
    for vm in vms:
        try:
            delete_vm_tags(logger, cloudstack, vm, {JOB_PREPARE_KEY: JOB_PREPARE_KEY})
        except cs.client.CloudStackApiException:
            pass
    jobs = []
    for vm in vms:
        try:
            job = cloudstack.destroyVirtualMachine(id=vm["id"], expunge=True)
        except cs.client.CloudStackApiException:
            vm_name = vm["name"]
            if get_vm_by_id(cloudstack, vm["id"]):
                raise Exception(f"Failed to destroy {vm_name}.")
            else:
                logger.info(
                    f"Virtual machine {vm_name} already destroyed.", exc_info=True
                )
                job = None
        if job:
            jobs.append((vm, job))
    while jobs != []:
        new_jobs = []
        for vm, job in jobs:
            try:
                cloudstack.queryAsyncJobResult(jobId=job["jobid"], fetch_result=True)
            except cs.client.CloudStackApiException:
                vm_name = vm["name"]
                if get_vm_by_id(cloudstack, vm["id"]):
                    logger.info(
                        f"Failed to destroy {vm_name}. Retrying.", exc_info=True
                    )
                    new_jobs.append(
                        (
                            vm,
                            cloudstack.destroyVirtualMachine(id=vm["id"], expunge=True),
                        )
                    )
                else:
                    logger.info(
                        f"Virtual machine {vm_name} already destroyed.", exc_info=True
                    )
        jobs = new_jobs


def recycle_vm(
    logger: logging.LoggerAdapter, cloudstack: cs.CloudStack, vm, job_info: JobInfo
) -> None:
    delete_vm_tags(
        logger,
        cloudstack,
        vm,
        {
            JOB_ID_TAG_KEY: job_info.job_id,
            JOB_TOKEN_TAG_KEY: job_info.job_token,
        },
    )
    create_vm_tags(logger, cloudstack, vm, {JOB_PREPARE_KEY: JOB_PREPARE_KEY})


def parse_image_name(
    image_name: str, default_spec: Spec, default_options: Options
) -> Image:
    """
    Parse image name.

    >>> parse_image_name("featured:ubuntu-2204-lts", DEFAULT_SPEC, DEFAULT_OPTIONS)
    Image(contents=CloudStack(kind='CloudStack', template_filter='featured', template_name='ubuntu-2204-lts', spec=Spec(cpu=2, ram=8, disk=16), options=Options(docker=None)))
    >>> parse_image_name("self:windows,cpu:8", DEFAULT_SPEC, DEFAULT_OPTIONS)
    Image(contents=CloudStack(kind='CloudStack', template_filter='self', template_name='windows', spec=Spec(cpu=8, ram=8, disk=16), options=Options(docker=None)))
    >>> parse_image_name("self:windows,cpu:8,ram:16", DEFAULT_SPEC, DEFAULT_OPTIONS)
    Image(contents=CloudStack(kind='CloudStack', template_filter='self', template_name='windows', spec=Spec(cpu=8, ram=16, disk=16), options=Options(docker=None)))
    >>> parse_image_name("community:ci.inria.fr/alpine-3.18.2-runner/amd64,cpu:8,docker:ubuntu", DEFAULT_SPEC, DEFAULT_OPTIONS)
    Image(contents=CloudStack(kind='CloudStack', template_filter='community', template_name='ci.inria.fr/alpine-3.18.2-runner/amd64', spec=Spec(cpu=8, ram=8, disk=16), options=Options(docker='ubuntu')))
    >>> parse_image_name("ubuntu,cpu:8", DEFAULT_SPEC, DEFAULT_OPTIONS)
    Image(contents=CloudStack(kind='CloudStack', template_filter='community', template_name='ci.inria.fr/alpine-3.18.2-runner/amd64', spec=Spec(cpu=8, ram=8, disk=16), options=Options(docker='ubuntu')))
    """
    cpu = default_spec.cpu
    ram = default_spec.ram
    disk = default_spec.disk
    docker = default_options.docker
    image_name, *options_list = image_name.split(",")
    try:
        prefix, template_name = image_name.split(":", 1)
    except ValueError:
        prefix = None
    if prefix == "custom-runner":
        return Image(contents=Test(kind="Test", name=template_name))
    if prefix in ["featured", "self", "community"]:
        template_filter = prefix
    else:
        template_filter = "community"
        template_name = "ci.inria.fr/alpine-3.18.2-runner/amd64"
        docker = image_name
    for option in options_list:
        try:
            name, value = option.split(":", 1)
        except ValueError:
            raise Exception(f"invalid option: {option}")
        try:
            match name:
                case "cpu":
                    cpu = int(value)
                    if cpu < 1 or cpu > MAX_CPU:
                        raise Exception(
                            f"cpu number should be between 1 and {MAX_CPU}: {cpu}"
                        )
                case "ram":
                    ram = int(value)
                    if ram < 1 or ram > MAX_RAM:
                        raise Exception(f"ram should be between 1 and {MAX_RAM}: {ram}")
                case "disk":
                    disk = int(value)
                    if disk < 1 or disk > MAX_DISK:
                        raise Exception(
                            f"disk should be between 1 and {MAX_DISK}: {disk}"
                        )
                case "docker":
                    docker = value
                case _:
                    raise Exception(f"invalid property {name}")
        except ValueError:
            raise Exception(f"invalid property value for {name}: {value}")
    spec = Spec(cpu=cpu, ram=ram, disk=disk)
    options = Options(docker=docker)
    return Image(
        contents=CloudStack(
            kind="CloudStack",
            template_filter=template_filter,
            template_name=template_name,
            spec=spec,
            options=options,
        )
    )


class CustomRunnerFormatter(logging.Formatter):
    def formatException(self, ei):
        """
        Does nothing since we don't want to log exception backtraces
        in GitLab logs.  These backtraces can still be useful in the
        logs we keep on custom runner orchestrator (indexed by
        elasticsearch).
        """
        pass


def exc_info_to_json():
    exc_type, exc_value, exc_traceback = sys.exc_info()

    # Extract traceback details
    tb_list = traceback.extract_tb(exc_traceback)
    structured_traceback = [
        {
            "filename": frame.filename,
            "line": frame.line,
            "lineno": frame.lineno,
            "name": frame.name,
        }
        for frame in tb_list
    ]

    # Convert exception information to a serializable format
    exc_data = {
        "type": exc_type.__name__ if exc_type else None,
        "message": str(exc_value) if exc_value else None,
        "traceback": structured_traceback if exc_traceback else None,
    }

    return exc_data


class CustomRunnerLoggerAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        """
        Extends the `extra` dictionary with the dictionary given in
        the constructor.  By default, `logging.LoggerAdapter` replaces
        whole the `extra` field by the dictionary given in the
        constructor, but we want to enable single log commands to add
        their own `extra` fields.
        """
        source = self.extra
        if kwargs.get("exc_info"):
            if source is None:
                source = {}
            else:
                source = dict(source)
            source["exception"] = exc_info_to_json()
        if source is not None:
            extra = kwargs.get("extra")
            if extra is None:
                kwargs["extra"] = source
            else:
                extra.update(source)
        return msg, kwargs


def init_loggers(name: str) -> logging.LoggerAdapter:
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARN)
    formatter = CustomRunnerFormatter("%(asctime)s | %(message)s")
    stream_handler.setFormatter(formatter)
    socket_handler = logging.handlers.SocketHandler(
        "localhost", logging.handlers.DEFAULT_TCP_LOGGING_PORT
    )
    socket_handler.setLevel(logging.INFO)
    logging.basicConfig(
        level=logging.INFO,
        handlers=[stream_handler, socket_handler],
    )
    try:
        extra = {
            "project_path": os.environ["CUSTOM_ENV_CI_PROJECT_PATH"],
            "pipeline_id": os.environ["CUSTOM_ENV_CI_PIPELINE_ID"],
            "job_id": os.environ["CUSTOM_ENV_CI_JOB_ID"],
        }
    except KeyError:
        extra = None
    logger = logging.getLogger(name)
    return CustomRunnerLoggerAdapter(logger, extra=extra)


def execute_with_loggers(
    name: str, f: typing.Callable[[logging.LoggerAdapter], None]
) -> None:
    adapter = init_loggers(name)
    try:
        f(adapter)
    except Exception as e:
        adapter.exception(f"Uncaught exception: {e}")
        raise e


def get_vm_disk_size(cloudstack: cs.CloudStack, vm) -> typing.Optional[int]:
    """
    return virtual machine disk size (in bytes)
    """
    try:
        return cloudstack.listVolumes(
            virtualmachineid=vm["id"], type="ROOT", fetch_list=True
        )[0]["size"]
    except IndexError:
        return None


def get_vm_spec(cloudstack: cs.CloudStack, vm) -> typing.Optional[Spec]:
    disk_size = get_vm_disk_size(cloudstack, vm)
    if disk_size is None:
        return None
    return Spec(
        cpu=vm["cpunumber"],
        ram=math.ceil(vm["memory"] / 1024),
        disk=math.ceil(disk_size / 1024**3),
    )


VM_NAME_PREFIX = "custom-runner-vm-"


def collect_cloudstack_metrics(cloudstack: cs.CloudStack):
    specs = [
        (vm, spec)
        for vm in list_virtual_machines(cloudstack, prefix=VM_NAME_PREFIX)
        for spec in [get_vm_spec(cloudstack, vm)]
        if spec is not None
    ]
    return {
        "total_vms": Spec.sum([spec for vm, spec in specs]).model_dump(),
        "used_vms": Spec.sum(
            [spec for vm, spec in specs if has_tag(vm, JOB_ID_TAG_KEY)]
        ).model_dump(),
        "prepared_vms": Spec.sum(
            [spec for vm, spec in specs if has_tag(vm, JOB_PREPARE_KEY)]
        ).model_dump(),
    }


def get_gitlab_job_info(job_token: str) -> requests.Response:
    return requests.get(f"{GITLAB_API_ENDPOINT}/job", params={"job_token": job_token})
