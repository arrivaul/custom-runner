set -ex

. infrastructure/init_ssh.sh

( echo $TF_VAR_SSH_PUBLIC_KEY; cat infrastructure/ssh-keys/* ) >authorized_keys

scp $SSH_OPTIONS authorized_keys ci@"$VM_NAME":.ssh/
