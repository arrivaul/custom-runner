set -ex

. infrastructure/init_ssh.sh

if [ -n "$TF_VAR_ZONE" ]; then
  ZONE="$TF_VAR_ZONE"
else
  ZONE="zone-ci"
fi

cat >config.yml <<EOF
cloudstack:
  endpoint: $CLOUDSTACK_API_URL
  key: $CLOUDSTACK_API_KEY
  secret: $CLOUDSTACK_SECRET_KEY
  timeout: 120
zone: $ZONE
EOF

if [ -n "$TF_VAR_CPU_SPEED" ]; then
  echo "cpu_speed: $TF_VAR_CPU_SPEED" >>config.yml
fi

while
  ! scp $SSH_OPTIONS -r src/* infrastructure/deploy/* config.yml \
    requirements.txt \
    ci@"$VM_NAME":
do
    sleep 10
done

if [ ! -n "$PROJECT_ID" ]; then
  PROJECT_ID="$CI_PROJECT_ID"
fi

ssh $SSH_OPTIONS ci@"$VM_NAME" sh deploy_gitlab-runner_on_vm.sh \
  "\"$RUNNER_TOKEN\"" "\"$ELASTIC_PASSWORD\"" \
  "\"$KIBANA_PASSWORD\"" "\"$RUNNER_NAME\"" \
  "\"$PROJECT_ID\"" "\"$PROJECT_ACCESS_TOKEN\""
