# How to contribute?

## Tests are always welcome!

The best way to help developing the custom runner is to test it.
Please follow the instructions given in the section [How to test the
custom
runner?](https://gitlab.inria.fr/inria-ci/custom-runner/#how-to-test-the-custom-runner)
in [README.md](README.md] to set up the custom runner for your
projects, and report any issue on the
[tracker](https://gitlab.inria.fr/inria-ci/custom-runner/-/issues).
Thank you very much!

## Infrastructure

The virtual machines are deployed in the project
[custom-runner](https://ci.inria.fr/project/custom-runner/show) hosted
on [ci.inria.fr](https://ci.inria.fr/).  The custom runner uses the
keys registered in the GitLab variables `CLOUDSTACK_API_KEY` and
`CLOUDSTACK_SECRET_KEY`: these keys belong to the user
`gitlabci-custom-runner@inria.fr` (a Sympa mailing-list).

### Creating your own custom-runner orchestrator

To run another instance of custom runner, fork the `custom-runner`
project on GitLab, create a new project on `ci.inria.fr` or
`qlf-ci.inria.fr`, and fill the following CI/CD variable:


(on cloudstack prod)

* `BOOTSTRAP_RUNNER_NAME`: the name of the runner used for deploying your own custom runner
* `CI_USER_NAME` : the name of an admin user of the CI project
* `CLOUDSTACK_API_KEY` : retrieved in the cloudstack web interface of an admin user of the CI project
* `CLOUDSTACK_API_URL` : the URL to the CloudStack instance to use,
  typically `https://sesi-cloud-ctl1.inria.fr/client/api`
* `CLOUDSTACK_SECRET_KEY` : retrieved in the cloudstack web interface of an admin user of the CI project
* `PROJECT_ACCESS_TOKEN` : gitlab project access token created via GitLab interface Settings, Access Tokens, select role Maintainer and 'api' scope): this token will only be used for the creation of the runner and saving authentication token in project settings, so you may choose a short expiration date;
* `PROJECT_ID` : the id of the gitlab project where to create orchestrator runner (if not set, the current project will be used)
* `SSH_PRIVATE_KEY` : the private key of the admin user of the CI project (associated to `TF_VAR_SSH_PUBLIC_KEY`)
* `TF_VAR_SSH_PUBLIC_KEY` : the public key registered on the CI portal for the admin user of the CI project

(on cloudstack qualif)

Pay attention to the following variables:

* `CLOUDSTACK_API_URL`: the URL to the CloudStack qualif instance to use,
  `https://qlf-sesi-cloud-ctl1.inria.fr/client/api`;
* if the zone is not `zone-ci`, set `TF_VAR_ZONE` accordingly (for
  instance, for `qlf-ci`, set `TF_VAR_ZONE` to `qlf-zone-ci`).
* if the SSH proxy is not `ci-ssh.inria.fr`, set `SSH_PROXY` and
  `TF_VAR_SSH_CIDR` accordingly (for instance, for `qlf-ci`, set
  `SSH_PROXY` to `qlf-ci-ssh.inria.fr` and set `TF_VAR_SSH_CIDR` to
  `172.21.0.247/32`).

## Preparing the execution of a job

For preparing the execution of a job,
`gitlab-runner` executes the script [`prepare.py`](src/prepare.py).

Before creating a virtual machine, the script first looks if a virtual
machine has already been prepared with the tag `prepare:prepare`: the
script first looks if such a virtual machine is already running, and
if no such machine is available, the script then looks if such a
virtual machine is already starting. The script “takes” the virtual
machine by adding a tag `job:[job_id]`. Since a tag cannot be added if
there is already a tag with the same name, this tag ensures that no
other jobs use the same virtual machine. Moreover, the `job` tag
allows the subsequent scripts [`run.py`](src/run.py) and
[`cleanup.py`](src/cleanup.py) to find the virtual machine from the
job id.

If no prepared virtual machine is available, the script tries to
instantiate a new virtual machine. If CloudStack quotas are exceeded,
then the script tries to find a set of prepared virtual machines to
destroy that will free enough resources to allow to instantiate the
virtual machine according to the specifications given for the job. The
script enumerates the prepared virtual machines by creation date (the
oldest first), until the sum of the specifications of the selected
virtual machines exceed the specifications of the requested virtual
machine. Then, the virtual machines that have been enumerated this way
are listed again from the most recent to the oldest one, removing
virtual machines that are not necessary to exceed the specifications
of the requested virtual machine. This double-check ensures that the
set of virtual machines that are destroyed is optimal in the sense
that any other possible sets contain virtual machines more recent than
the virtual machines that have been selected. Before the virtual
machines are destroyed, the tag `job:destroy` is added and the tag
`prepare:prepare` is removed, so that the virtual machines cannot be
taken by another jobs before destruction (if the tag `job:destroy`
cannot be added, that means that the job has been taken or selected
for destruction meanwhile, and the virtual machine is
ignored). Virtual machines in error are destroyed too. If no virtual
machines can be destroyed, the script is paused for 10s. Then, the
script loops for trying reusing a virtual machine or creating a new
one.

## Clean up and preparation of virtual machines

Once a job is cleaned up (either on success, failure or cancellation),
`gitlab-runner` executes the script [`cleanup.py`](src/cleanup.py).

If the virtual machine associated to the job has never been connected
to (there is no tag `runner:initialized`), the script just removes the
`job` tag and add a tag `prepare:prepare` to mark the virtual machine
as prepared. Therefore, if a job is interrupted during the creation of
the virtual machine, the virtual machine can be used by another job.

Otherwise, the script sends the job information through the
[∅MQ](https://zeromq.org/languages/python/) socket
`ipc:///builds/destroy.socket`, so that the `systemd` service
[`destroy.py`](src/destroy.py) destroys the virtual machine.
Destroying the virtual machine is not performed by the script
`cleanup.py` itself, so as to terminate the job as quicly as possible
for GitLab.

The script `destroy.py` destroys the virtual machine associated to the
job and instantiates a virtual machine with the same specification
again if CloudStack quotas allow it. The virtual machine is tagged
`prepare:prepare` to mark the virtual machine as prepared.

## OpenTofu

We use open-source platform [OpenTofu](https://opentofu.org/) as an
alternative to Terraform. This decision was made after Terraform was
removed from Alpine v3.19, as detailed in the [Alpine Linux Release
Notes for v3.19.0](https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.19.0).

## CloudStack VM deployment disabled on CloudStack error `530 Insufficient capacity`

CloudStack VM deployment is disabled on CloudStack error `530
Insufficient capacity`: any subsequent VM deployment is blocked before
a manual intervention (a file `/builds/vm_deployment_disabled` is
created: while such a file exists, `deploy_vm` immediately breaks the
script).
See https://gitlab.inria.fr/inria-ci/custom-runner/-/issues/17
